CREATE DATABASE usuarios;
\c usuarios

CREATE SEQUENCE actions_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE 
	START WITH 1
	NO CYCLE;

CREATE TABLE actions(
  id INTEGER NOT NULL DEFAULT nextval('actions_seq'),
  accion VARCHAR(50) NOT NULL,
  CONSTRAINT pk_movimiento PRIMARY KEY (id)
);

CREATE SEQUENCE tipos_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE 
	START WITH 1
	NO CYCLE;

CREATE TABLE tipos(
	id INTEGER NOT NULL DEFAULT nextval('tipos_seq'),
	tipo VARCHAR(13) NOT NULL,
	CONSTRAINT pk_tipos PRIMARY KEY (id)
);

CREATE SEQUENCE estatus_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE 
	START WITH 1
	NO CYCLE;

CREATE TABLE estatus(
	id INTEGER NOT NULL DEFAULT nextval('estatus_seq'),
	estatus VARCHAR(13) NOT NULL,
	CONSTRAINT pk_estatus PRIMARY KEY (id)
);

CREATE SEQUENCE users_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE 
	START WITH 1
	NO CYCLE;

CREATE TABLE users(
	id INTEGER NOT NULL DEFAULT nextval('users_seq'),
	nombre VARCHAR(30) NOT NULL,
	aPaterno VARCHAR(30) NOT NULL,
	aMaterno VARCHAR(30),
	email VARCHAR(35) NOT NULL,
	password VARCHAR(60) NOT NULL,
	image VARCHAR(255) NULL,
	imageDir VARCHAR(255) NULL,
	estatus INTEGER NOT NULL,
	tipo INTEGER NOT NULL,
	CONSTRAINT pk_user PRIMARY KEY(id),
	CONSTRAINT fk_estatus FOREIGN KEY (estatus) REFERENCES estatus(id),
	CONSTRAINT fk_tipo FOREIGN KEY (tipo) REFERENCES tipos(id)
);

CREATE SEQUENCE register_seq
	INCREMENT BY 1
	MINVALUE 1
	NO MAXVALUE 
	START WITH 1
	NO CYCLE;

CREATE TABLE registers(
  id INTEGER NOT NULL DEFAULT nextval('register_seq'),
  usuario INTEGER NOT NULL,
  accion INTEGER NOT NULL,
  fecha TIMESTAMPTZ,
  CONSTRAINT pk_registro PRIMARY KEY (id),
  CONSTRAINT fk_usuario FOREIGN KEY(usuario) REFERENCES users(id) ON DELETE CASCADE,
  CONSTRAINT fk_accion FOREIGN KEY (accion) REFERENCES actions(id)
);

INSERT INTO tipos(tipo) VALUES('Administrador');
INSERT INTO tipos(tipo) VALUES('Usuario');
INSERT INTO estatus(estatus) VALUES('Habilitado');
INSERT INTO estatus(estatus) VALUES('Deshabilitado');
INSERT INTO users(nombre, aPaterno, aMaterno, email, password, estatus, tipo) VALUES('Juan','Bravo', 'Castro', 'jbravo@correo.com', '$2y$10$Hz/BAt2o5VYG47UXcnGrHeZLIb8gTN68mvALMhkOP.jm2fRX1er3a', 1, 1); --password
INSERT INTO users(nombre, aPaterno, aMaterno, email, password, estatus, tipo) VALUES('Victor','Ruíz', 'Albor', 'vic@correo.com', '$2y$10$8Aa3TG7/4Y9k8MuzTI/mHO.NIHHrzcLVaCRA0vJS9YgQo8C9SGbii', 1, 1); --sekret
INSERT INTO actions(accion) VALUES('Login');
INSERT INTO actions(accion) VALUES('Logout');
INSERT INTO actions(accion) VALUES('Registró usuario');
INSERT INTO actions(accion) VALUES('Modificó usuario');
INSERT INTO actions(accion) VALUES('Eliminó usuario');
INSERT INTO actions(accion) VALUES('Restableció contraseña');
INSERT INTO actions(accion) VALUES('Cambió contraseña');