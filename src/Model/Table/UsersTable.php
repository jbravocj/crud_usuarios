<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Model
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 */
class UsersTable extends Table {
    public function initialize(array $config){
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Proffer.Proffer', [
            'image' => [	// The name of your upload field
                    'root' => WWW_ROOT . 'files', // Customise the root upload folder here, or omit to use the default
                    'dir' => 'imagedir',	// The name of the field to store the folder
                    'thumbnailSizes' => [ // Declare your thumbnails
                            'square' => [	// Define the prefix of your thumbnail
                                    'w' => 200,	// Width
                                    'h' => 200,	// Height
                                    'jpeg_quality'	=> 100
                            ],
                            'portrait' => [		// Define a second thumbnail
                                    'w' => 100,
                                    'h' => 300
                            ],
                    ],
                    'thumbnailMethod' => 'gd'	// Options are Imagick or Gd
            ]
        ]);
    }

    public function validationDefault(Validator $validator){
        $validator->integer('id')->allowEmptyString('id', 'create');

        $validator->scalar('nombre')->minLength('nombre', 4)->maxLength('nombre', 30)->requirePresence('nombre', 'create')->allowEmptyString('nombre', false, 'No puedes dejar el campo vacío')
            ->add('nombre', 'validFormat', ['rule' => array('custom', '/[A-Z a-zÁÉÍÓÚáéíóúÑñ]$/i'), 'message' => 'Por favor ingrese un nombre válido']);

        $validator->scalar('apaterno')->minLength('apaterno', 3)->maxLength('apaterno', 30)->requirePresence('apaterno', 'create')->allowEmptyString('apaterno', false, 'No puedes dejar el campo vacío')
            ->add('apaterno', 'validFormat', ['rule' => array('custom', '/[A-Z a-zÁÉÍÓÚáéíóúÑñ]$/i'), 'message' => 'Por favor ingrese un apellido válido']);

        $validator->scalar('amaterno')->minLength('amaterno', 3)->maxLength('amaterno', 30)->allowEmptyString('amaterno', 'No puedes dejar el campo vacío')
            ->add('amaterno', 'validFormat', ['rule' => array('custom', '/[A-Z a-zÁÉÍÓÚáéíóúÑñ]$/i'), 'message' => 'Por favor ingrese un apellido válido']);

        $validator->email('email')->maxLength('email', 30)->requirePresence('email', 'create')->allowEmptyString('email', false, 'No puedes dejar el campo vacío');
        $validator->integer('estatus')->requirePresence('estatus', 'create')->allowEmptyString('estatus', false);
        $validator->integer('tipo')->requirePresence('tipo', 'create')->allowEmptyString('tipo', false);

        $validator->scalar('password')->minLength('password', 6)->maxLength('password', 60)->requirePresence('password', 'create')->allowEmptyString('password', false, 'No puedes dejar el campo vacío');
        $validator->sameAs('password', 'passwordB', 'Las contraseñas no coinciden')->allowEmptyString('passwordB', false, 'No puedes dejar el campo vacío');
        $validator->sameAs('passwordNueva', 'passwordNuevaB', 'Las nuevas contraseñas no coinciden.')->allowEmptyString('passwordNueva', false, 'No puedes dejar el campo vacío');
        
        $validator->allowEmptyString('passwordAntigua', false, 'No puedes dejar el campo vacío');
        $validator->allowEmptyString('passwordNuevaB', false, 'No puedes dejar este campo vacío');
        
        $validator->add('passwordAntigua','custom',['rule'=>  function($value, $context){
                        $user = $this->get($context['data']['id']);
                        if ($user) {
                            if ((new DefaultPasswordHasher)->check($value, $user->password)) { return true; }
                        }
                        return false;
                        },'message' => 'La contraseña actual no coincide.']);
        return $validator;
    }

    public function buildRules(RulesChecker $rules){
        $rules->add($rules->isUnique(['email']));
        return $rules;
    }
    
    public function findAuth(\Cake\ORM\Query $query, array $options){
        $query->select(['id', 'nombre', 'apaterno', 'amaterno', 'email', 'estatus', 'tipo', 'password'])->where(['Users.estatus' => 1]);
        return $query;
    }
}