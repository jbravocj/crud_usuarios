<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <title>Recuperación de contraseña</title>
</head>
<body>
    <div class="container-fluid">
        <nav class="navbar navbar-light bg-dark navbar-dark" style="margin-bottom: 25px;">
            <a href="localhost/asignacion/Users/login" class="navbar-brand">Asignación CRUD de usuarios</a>
        </nav>
    </div>
    <div class="container">
        <h3 class="text-center">Hola, <?= $nombre?></h3><br>
        <p class="text-justify">Hemos visto que has perdido tu contraseña. No te preocupes, ahora podrás ingresar de nuevo al sistema con la siguiente contraseña.</p>
        <div class="card" style="width: 18rem;">
          <div class="card-header">Nueva contraseña</div>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">Contraseña <h4><?= $password?></h4></li>
          </ul>
        </div>
        <p>Al ingresar podrás cambiar tu contraseña de nuevo. Saludos y sé cuidadoso con tu contraseña.</p>
    </div>
</body>
</html>