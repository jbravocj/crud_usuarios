<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        <title>Usuarios</title>
    </head>
    <body>
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a><?= $this->Html->link('Inicio', ['controller' => 'users'], array('class' => 'navbar-brand')) ?></a>
                <div class="collapse navbar-collapse" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item">
                        <a><?= $this->Html->link(__('Tipos de movimiento'), ['controller' => 'actions'], array('class' => 'nav-link')) ?></a>
                    </li>
                  </ul>
                </div>
                <a class="pull-right">Bienvenido, <?= $this->getRequest()->getSession()->read('Auth.User.nombre')?></a>
            </nav>
            <h3 class="text-center"><?= __('Logs') ?></h3><br>
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('usuario') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('accion') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('fecha') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($registers as $register): ?>
                    <tr>
                        <td><?= $this->Number->format($register->id) ?></td>
                        <td><?= h($register['Users']['nombre']. ' ' . $register['Users']['apaterno'] . ' ' . $register['Users']['amaterno']) ?></td>
                        <td><?= h($register['Actions']['accion']) ?></td>
                        <td><?= h($register->fecha) ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="pagination pagination-centered right">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('primero')) ?>
                    <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('siguiente') . ' >') ?>
                    <?= $this->Paginator->last(__('último') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} de un total de {{count}} resultados')]) ?></p>
            </div>
        </div>
    </body>
</html>