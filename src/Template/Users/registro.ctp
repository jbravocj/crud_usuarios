<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        <title>Registro</title>
    </head>
    <body>
        <div>
            <?= $this->Form->create($user, ['type' => 'file']) ?>
            <fieldset>
                <legend class="text-center"><?= 'Registro' ?></legend>
                <?= $this->Form->control('nombre') ?>
                <?= $this->Form->input('apaterno', array('label' => 'Apellido Paterno')) ?>
                <?= $this->Form->input('amaterno', array('label' => 'Apellido Materno')) ?>
                <?= $this->Form->control('email') ?>
                <?= $this->Form->input('image', ['type' => 'file']) ?>
                <?= $this->Recaptcha->display() ?>
                <caption>*Se te enviará al correo que registraste un email que contiene tu contraseña para ingresar al sistema. Si ya tienes cuenta, por favor, <a><?= $this->Html->link('inicia sesión.', ['action' => 'login']) ?></a></caption>
            </fieldset>
            <?= $this->Form->button(__('Registrarme'), array('class' => 'right btn btn-lg btn-success', 'style' => 'margin-top: -85px;')) ?>
            <?= $this->Form->end() ?>
        </div>
    </body>
</html>