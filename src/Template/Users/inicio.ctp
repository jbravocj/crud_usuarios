<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        <title>Inicio</title>
    </head>
    <body>
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a><?= $this->Html->link('Inicio', ['controller' => 'users', 'action' => 'inicio'], array('class' => 'navbar-brand')) ?></a>
                <div class="collapse navbar-collapse" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item">
                      <a><?= $this->Html->link(__('Cambiar contraseña'), ['action' => 'cambiarPassword', $usuario->read('Auth.User.id')], array('class' => 'nav-link')) ?></a>
                    </li>
                  </ul>
                </div>
                <a class="pull-right">Bienvenido, <?= $usuario->read('Auth.User.nombre')?></a>
            </nav><br>
        </div>
        <div class="container">
            <div class="card mx-auto" style="width: 25rem;">
                <div class="card-header text-center"><?= $nombreCompleto ?></div>
                <?= $this->Html->image('../files/users/image/' . $foto->imagedir . '/' . $foto->image, ['alt' => 'Imagen del usuario', 'class' => 'card-img-top', 'style' => 'height:25rem', 'style' => 'width:25rem']); ?>
                <div class="card-body">
                    <h5 class="card-title">Correo electrónico</h5>
                    <p class="card-text"><?= $usuario->read('Auth.User.email') ?></p>
                    <?= $this->Html->link('Cambiar contraseña', ['action' => 'cambiarPassword', $usuario->read('Auth.User.id')], array('class' => 'btn-sm btn-primary right', 'style' => 'margin-top:-78px;')); ?>
                </div>
            </div>
        </div>
    </body>
</html>