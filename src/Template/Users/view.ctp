<html><?php //debug($user);?>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        <title>Usuario</title>
    </head>
    <body>
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a><?= $this->Html->link('Inicio', ['controller' => 'users'], array('class' => 'navbar-brand')) ?></a>
                <div class="collapse navbar-collapse" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item">
                      <a><?= $this->Html->link(__('Editar usuario'), ['action' => 'edit', $user->id], array('class' => 'nav-link')) ?></a>
                    </li>
                    <li class="nav-item">
                      <a><?= $this->Form->postLink(__('Borrar usuario'), ['action' => 'delete', $user->id], ['confirm' => __('¿Estás seguro que deseas eliminar al usuario número {0}?', $user->id), 'class' => 'nav-link']) ?></a>
                    </li>
                    <li class="nav-item">
                      <a><?= $this->Html->link(__('Agregar usuario'), ['action' => 'add'], array('class' => 'nav-link')) ?></a>
                    </li>
                  </ul>
                </div>
            </nav>
            <div class="card">
                <div class="card-header text-center"><h3><?= h($user->nombre) . ' ' . h($user->apaterno) . ' ' . h($user->amaterno) ?></h3></div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Número de usuario <h4><?= h($user->id) ?></h4></li>
                    <li class="list-group-item">Correo electrónico <h4><?= h($user->email) ?></h4></li>
                    <li class="list-group-item">Contraseña <h4><?= h($user->password) ?></h4></li>
                    <li class="list-group-item">Estatus <h4><?= $user['Estatus']['estatus'] ?></h4></li>
                    <li class="list-group-item">Tipo de usuario <h4><?= $user['Tipos']['tipo'] ?></h4></li>
                </ul>
            </div>
        </div>
    </body>
</html>