<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        <title>Usuarios</title>
    </head>
    <body>
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a><?= $this->Html->link('Inicio', ['controller' => 'users'], array('class' => 'navbar-brand')) ?></a>
                <div class="collapse navbar-collapse" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item">
                      <a><?= $this->Html->link(__('Nuevo usuario'), ['action' => 'add'], array('class' => 'nav-link')) ?></a>
                    </li>
                    <li class="nav-item">
                      <a><?= $this->Html->link(__('Logs'), ['controller' => 'registers'], array('class' => 'nav-link')) ?></a>
                    </li>
                    <li class="nav-item">
                      <a><?= $this->Html->link(__('Cambiar contraseña'), ['action' => 'cambiarPassword', $this->getRequest()->getSession()->read('Auth.User.id')], array('class' => 'nav-link')) ?></a>
                    </li>
                  </ul>
                </div>
                <a class="pull-right">Bienvenido, <?= $this->getRequest()->getSession()->read('Auth.User.nombre')?></a>
            </nav>
            <h3 class="text-center"><?= __('Usuarios') ?></h3><br>
            <table class="table">
                <thead class="thead-light">
                  <tr>
                    <th scope="col"><?= $this->Paginator->sort('apaterno', 'Apellido Paterno') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('amaterno', 'Apellido Materno') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('nombre') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('estatus') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('tipo') ?></th>
                    <th scope="col" class="actions"><?= __('Acciones') ?></th>
                  </tr>
                </thead>
                <tbody><?php foreach ($users as $user): ?>
                  <tr>
                    <td><?= h($user->apaterno) ?></td>
                    <td><?= h($user->amaterno) ?></td>
                    <td><?= h($user->nombre) ?></td>
                    <td><?= h($user->email) ?></td>
                    <td><?= h($user['Estatus']['estatus']) ?></td>
                    <td><?= h($user['Tipos']['tipo']) ?></td>
                    <td style="white-space: nowrap;"><!-- The buttons are already displayed as inline-block. Maybe the table isn't wide enough. Use this(white-space: nowrap)in style. -->
                        <?= $this->Html->link('Ver', ['action' => 'view', $user->id], array('class' => 'btn btn-outline-secondary btn-sm', 'style' => 'margin-left: -20px;')) ?>
                        <?= $this->Html->link('Editar', ['action' => 'edit', $user->id], array('class' => 'btn btn-outline-primary btn-sm')) ?>
                        <?= $this->Form->postLink('Borrar', ['action' => 'delete', $user->id], ['confirm' => __('¿Estás seguro que deseas borrar al usuario número {0}?', $user->id), 'class' => 'btn btn-outline-danger btn-sm']) ?>
                    </td>
                  </tr><?php endforeach; ?>
                </tbody>
              </table>
            <div class="pagination pagination-centered right">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('primero')) ?>
                    <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('siguiente') . ' >') ?>
                    <?= $this->Paginator->last(__('último') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} de un total de {{count}} resultados')]) ?></p>
            </div>
        </div>
    </body>
</html>