<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        <title>Restablecer contraseña</title>
        <script src="../../js/jquery-1.11.2.min.js"></script>
        <script src='https://www.google.com/recaptcha/api.js?render=6Lca9YoUAAAAACfUvsMxOksKm-dJ-r1zB6IbDCjx'></script>
    </head>
    <body>
        <h1 class="text-center">Restablecer contraseña</h1>
        <p class="text-center">Ingresa tu correo electrónico para que podamos enviarte tu contraseña restablecida por email, por favor o <a><?= $this->Html->link(__('iniciar sesión.'), ['action' => 'login']) ?></a></p>
        <?= $this->Form->create($user) ?>
        <?= $this->Form->control('emailRev', ['label' => ['text' => 'Correo electrónico']]) ?>
        <?= $this->Recaptcha->display() ?>
        <?= $this->Form->button('Enviar', array('class' => 'right btn btn-lg btn-primary', 'style' => 'margin-top: -55px;')) ?>
        <?= $this->Form->end() ?>
    </body>
</html>
