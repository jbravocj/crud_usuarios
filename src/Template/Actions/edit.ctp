<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        <title>Editar usuario</title>
    </head>
    <body>
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a><?= $this->Html->link('Inicio', ['controller' => 'users'], array('class' => 'navbar-brand')) ?></a>
                <div class="collapse navbar-collapse" id="navbarNav">
                  <ul class="navbar-nav">
                    <li><?= $this->Html->link(__('Lista de movimientos'), ['action' => 'index'], array('class' => 'nav-link')) ?></li>
                  </ul>
                  <ul class="navbar-nav">
                    <li class="nav-item">
                      <a><?= $this->Form->postLink(__('Borrar movimiento'), ['action' => 'delete', $action->id], ['confirm' => __('¿Estás seguro que deseas eliminar el movimiento número {0}?', $action->id), 'class' => 'nav-link']) ?></a>
                    </li>
                  </ul>
                </div>
            </nav>
        </div>
        <div class="container">
            <legend class="text-center"><?= __('Editar movimiento') ?></legend>
            <?= $this->Form->create($action) ?>
            <fieldset>
                <?php echo $this->Form->control('accion'); ?>
            </fieldset><br>
            <?= $this->Html->link(__('Cancelar'), $this->request->referer(), ['class' => 'right btn btn-lg btn-danger', 'style' => 'margin-left: 10px;']) ?>
            <?= $this->Form->button(__('Guardar'), array('class' => 'right btn btn-lg btn-success')) ?>
            <?= $this->Form->end() ?>
        </div>
    </body>
</html>