<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        <title>Usuarios</title>
    </head>
    <body>
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a><?= $this->Html->link('Inicio', ['controller' => 'users'], array('class' => 'navbar-brand')) ?></a>
                <div class="collapse navbar-collapse" id="navbarNav">
                  <ul class="navbar-nav">
                    <li class="nav-item">
                        <a><?= $this->Html->link(__('Agregar Movimiento'), ['action' => 'add'], array('class' => 'nav-link')) ?></a>
                    </li>
                    <li class="nav-item">
                      <a><?= $this->Html->link(__('Logs'), ['controller' => 'registers'], array('class' => 'nav-link')) ?></a>
                    </li>
                  </ul>
                </div>
                <a class="pull-right">Bienvenido, <?= $this->getRequest()->getSession()->read('Auth.User.nombre')?></a>
            </nav>
            <h3 class="text-center"><?= __('Tipos de movimientos') ?></h3><br>
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('accion', 'Movimiento') ?></th>
                        <th scope="col" class="actions text-center"><?= __('Acciones') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($actions as $action): ?>
                    <tr>
                        <td><?= $this->Number->format($action->id) ?></td>
                        <td><?= h($action->accion) ?></td>
                        <td class="actions text-center">
                            <?= $this->Html->link('Editar', ['action' => 'edit', $action->id], array('class' => 'btn btn-outline-primary btn-sm')) ?>
                            <?= $this->Form->postLink('Borrar', ['action' => 'delete', $action->id], ['confirm' => __('¿Estás seguro que deseas borrar el tipo de movimiento {0}?', $action->id), 'class' => 'btn btn-outline-danger btn-sm']) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="pagination pagination-centered right">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('primero')) ?>
                    <?= $this->Paginator->prev('< ' . __('anterior')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('siguiente') . ' >') ?>
                    <?= $this->Paginator->last(__('último') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} de un total de {{count}} resultados')]) ?></p>
            </div>
        </div>
    </body>
</html>