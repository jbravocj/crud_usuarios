<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize(){
        parent::initialize();
        
        $this->loadComponent('Recaptcha.Recaptcha', [
            'enable' => true,     // true/false
            'sitekey' => '6Lde_ooUAAAAALGtJnrcQByeerCAsiAx23lnQEWD', //if you don't have, get one: https://www.google.com/recaptcha/intro/index.html
            'secret' => '6Lde_ooUAAAAABmVkMvo8NgoC3_ELuNsfLhXh22A',
            'type' => 'image',  // image/audio
            'theme' => 'light', // light/dark
            'lang' => 'en',      // default en
            'size' => 'normal'  // normal/compact
        ]);

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        
        $this->loadComponent('Auth', [
            'authorize' => ['Controller'], //Esta línea es muy importante
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email', 'email', 'password' => 'password'],
                    'userModel' => 'Users',
                    'finder' => 'auth'
                ],
            ],
            'loginRedirect' => [
                'controller' => 'Users',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'storage' => 'Session',
            'authError' => 'No tienes permisos para entrar aquí.',
            'unauthorizedRedirect' => ['controller' => 'Users', 'action' => 'inicio']//Aquí redirijo si no hay login o si el usuario no tiene permisos
        ]);

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
    }
    
    public function beforeFilter(Event $event){
        $this->Auth->allow(['registro', 'login', 'forgotPassword']);
    }
}