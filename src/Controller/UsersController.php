<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;

class UsersController extends AppController {
    public $paginate = ['limit' => 10, 'order' => ['Users.apaterno' => 'asc']];

    public function beforeFilter(Event $event){ 
        parent::beforeFilter($event); 
    }
    
    public function generateRandomString($length = 10) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+-!#/()=?', ceil($length/strlen($x)) )),1,$length);
    }
    
    public function login(){
        if($this->Auth->user()){
            $this->Flash->error(__('Ya has iniciado sesión')); return $this->redirect('/Users');
        }
        if ($this->request->is('post') && $this->Recaptcha->verify()) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                $this->registrarLog(1, $user['id']);
                if ($user['tipo'] == 1) {
                    return $this->redirect($this->Auth->redirectUrl());
                } else if ($user['tipo'] == 2) {
                    return $this->redirect(['controller' => 'Users', 'action' => 'inicio']);
                }
            } else {
                if ($user['estatus'] == 2) {
                    $this->Flash->error('Lo sentimos, eres un usuario deshabilitado.');
                }
            }
            $this->Flash->error('Hubo un error al iniciar sesión. Es posible que tu correo y/o contraseña sean incorrectas.');
        }
    }
    
    public function logout(){ 
        $this->Flash->success('Has cerrado tu sesión.');
        $this->registrarLog(2, $this->getRequest()->getSession()->read('Auth.User.id'));
        return $this->redirect($this->Auth->logout());
    }
    
    public function inicio(){
        $usuario = $this->getRequest()->getSession();
        $nombreCompleto = $usuario->read('Auth.User.nombre') . ' ' . $usuario->read('Auth.User.apaterno') . ' ' . $usuario->read('Auth.User.amaterno');
        $foto = $this->Users->findById($usuario->read('Auth.User.id'))->select(['imagedir', 'image'])->first();
        $this->set(compact('usuario', 'nombreCompleto', 'foto'));
    }
    
    public function index(){
        $users = $this->Users->find()->select
                (['id', 'nombre', 'apaterno', 'amaterno', 'email', 'password', 'Estatus.estatus', 'Tipos.tipo'])
                ->join(['table' => 'tipos', 'alias' => 'Tipos',
                    'type' => 'INNER', 'conditions' => 'Users.tipo = Tipos.id'])
                ->join(['table' => 'estatus', 'alias' => 'Estatus',
                    'type' => 'INNER', 'conditions' => 'Users.estatus = Estatus.id']);
        $this->paginate($users);
        $this->set(compact('users'));
    }
    
    public function view($id = null){
        $user= $this->Users->find()
                ->join(['table' => 'tipos', 'alias' => 'Tipos',
                    'type' => 'INNER', 'conditions' => 'Users.tipo = Tipos.id'])
                ->join(['table' => 'estatus', 'alias' => 'Estatus',
                    'type' => 'INNER', 'conditions' => 'Users.estatus = Estatus.id'])
                ->where([
                'Users.id' => $id])
                ->select(['Users.id', 'Users.nombre', 'Users.apaterno', 'Users.amaterno', 'Users.email', 'Users.password', 'Estatus.estatus', 'Tipos.tipo'])
                ->first();
        
        $this->set(compact('user'));
    }
    
    public function add() {
        $pass = $this->generateRandomString();
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->estatus = 1;
            $user->password = $pass;
            $user->passwordB = $pass;
            if ($this->Users->save($user)) {
                $this->registrarLog(3, $this->getRequest()->getSession()->read('Auth.User.id'));
                $this->Flash->success(__('Se ha guardado correctamente al usuario.'));
                $this->enviarCorreo($user, $pass, 'plantilla', 'Registro exitoso');
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El usuario no pudo ser guardado.'));
        }
        $this->set(compact('user'));
    }

    public function edit($id = null) {
        $user = $this->Users->get($id, [ 'contain' => [] ]);
        $user->passwordB = $user->password;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->registrarLog(4, $this->getRequest()->getSession()->read('Auth.User.id'));
                $this->Flash->success(__('Se ha actualizado el usuario con éxito.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se ha podido actualizar el usuario.'));
        }
        $this->set(compact('user'));
    }

    public function delete($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('El usuario ha sido borrado con éxito.'));
        } else {
            $this->Flash->error(__('El usuario no ha podido ser borrado. Intente de nuevo, por favor.'));
        }
        $this->registrarLog(5, $this->getRequest()->getSession()->read('Auth.User.id'));
        return $this->redirect(['action' => 'index']);
    }
    
    public function forgotPassword() {
        $this->isLoged("Ya has iniciado sesión. Para cambiar tu contraseña, da click al botón 'Cambiar contraseña'");
        $user = $this->Users->newEntity();
        if ($this->request->is('post') && $this->Recaptcha->verify()) {
            $pass = $this->generateRandomString();
            $usr = $this->Users->findByEmail($this->request->data['emailRev'])->first();
            if (empty($usr)) {
                $this->Flash->error('La dirección de correo que has inroducido no existe. Intenta de nuevo, por favor.');
                return $this->redirect(['controller' => 'Users', 'action' => 'login']);
            } 
            $this->Flash->success('Correo encontrado ->' . $usr->email);
            $this->Users->patchEntity($usr, $this->request->getData());
            $usr->password = $pass;
                if ($this->Users->save($usr)) {
                    $this->registrarLog(6, $usr['id']);
                    $this->Flash->success(__('Se ha actualizado y enviado tu nueva contraseña, revisa tu correo, por favor.'));
                    $this->enviarCorreo($usr, $pass, 'recuperacion', 'Restablecimiento de contraseña');
                    return $this->redirect(['action' => 'login']);
                }
        }
        $this->set(compact('user'));
    }
    
    public function cambiarPassword($id){
        $user = $this->Users->get($id, ['contain' => []]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->password = $this->request->data['passwordNueva'];
            if ($this->Users->save($user)) {
                $this->registrarLog(7, $this->getRequest()->getSession()->read('Auth.User.id'));
                $this->Flash->success(__('Se ha guardado tu nueva contraseña con éxito.'));
                ($this->getRequest()->getSession()->read('Auth.User.tipo') === 1) ? $this->redirect(['action' => 'index']) : $this->redirect(['action' => 'inicio']);
            } else {
                $this->Flash->error(__('Lo sentimos, no se ha podido guardar tu nueva contraseña. Intenta de nuevo, por favor.'));
            }
        }
        $this->set(compact('user'));
    }
    
    public function registro(){
        $this->isLoged("Ya has iniciado sesión, no puedes registrar una cuenta nueva ahora.");
        $pass = $this->generateRandomString();
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->password = $pass;
            $user->passwordB = $pass;
            $user->estatus = 1; //Habilitado
            $user->tipo = 2; //Usuario, el administrador es igual a 1
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Se ha completado correctamente tu registro.'));
                $this->enviarCorreo($user, $pass, 'plantilla', 'Registro exitoso');
                return $this->redirect(['action' => 'login']);
            }
            $this->Flash->error(__('Lo sentimos. No se ha podido completar correctamente tu registro.'));
        }
        $this->set(compact('user'));
    }
    
    public function isAuthorized($user){
        /*if (in_array($this->action, array('cambiarPassword'))) {
            $id = $this->getRequest()->getSession()->read('Auth.User.id');
            if ($this->User == $id) { return true; } }*/
        if ($user['tipo'] != 1) {
            $allowedActions = ['inicio', 'logout', 'cambiarPassword'];
            if (in_array($this->request->action, $allowedActions)) { return true; }
        }
        if ($user['tipo'] == 1) { return true; }
    }
    //Funciones auxiliares para comprobar si el usuario se logeo, registrar logs y enviar correos.
    public function isLoged($mensajeFlash = ""){
        if($this->Auth->user()){
            $this->Flash->error($mensajeFlash);
            return $this->redirect(['controller' => 'Users', 'action' => 'inicio']);
        }
    }
    
    public function registrarLog($accion, $user){
        $registro = TableRegistry::get('Registers');
        $query = $registro->query();
        $query->
            insert(['usuario', 'accion', 'fecha'])
                ->values([
                    'usuario' => $user,
                    'accion' => $accion,
                    'fecha'=>'Now()'])->execute();
    }
    
    public function enviarCorreo($user, $pass, $plantilla = "", $subject = ""){
        $correo = new Email();
        $correo->transport('default')
               ->template($plantilla)
               ->emailFormat('html')
               ->to($user->email)
               ->subject($subject)
               ->viewVars(['nombre' => $user->nombre,
                           'apaterno' => $user->apaterno,
                           'amaterno' => $user->amaterno,
                           'email' => $user->email,
                           'password' => $pass]);
        if($correo->send()){
            $this->Flash->success(__('La contraseña ha sido enviada al nuevo correo registrado'));
        } else {
            $this->Flash->success(__('Error al envíar correo con contraseña del usuario.'));
        }
    }
}