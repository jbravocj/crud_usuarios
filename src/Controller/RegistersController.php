<?php
namespace App\Controller;

use App\Controller\AppController;

class RegistersController extends AppController {
    public $paginate = ['limit' => 10, 'order' => ['Registers.fecha' => 'asc']];
    
    public function index() {
        $registers = $this->Registers->find()->select
                (['id', 'Users.nombre', 'Users.apaterno', 'Users.amaterno', 'Actions.accion', 'fecha'])
                ->join(['table' => 'users', 'alias' => 'Users',
                    'type' => 'INNER', 'conditions' => 'Users.id = Registers.usuario'])
                ->join(['table' => 'actions', 'alias' => 'Actions',
                    'type' => 'INNER', 'conditions' => 'Actions.id = Registers.accion']);
        $this->paginate($registers);
        $this->set(compact('registers'));
    }
    
    public function isAuthorized($user){
        if ($user['tipo'] == 1) { 
            return true; 
        }
    }
}