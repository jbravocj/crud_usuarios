<?php
namespace App\Controller;

use App\Controller\AppController;

class ActionsController extends AppController {
    public function index(){
        $actions = $this->paginate($this->Actions);
        $this->set(compact('actions'));
    }

    public function view($id = null) {
        $action = $this->Actions->get($id, [
            'contain' => []
        ]);

        $this->set('action', $action);
    }

    public function add() {
        $action = $this->Actions->newEntity();
        if ($this->request->is('post')) {
            $action = $this->Actions->patchEntity($action, $this->request->getData());
            if ($this->Actions->save($action)) {
                $this->Flash->success(__('The action has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The action could not be saved. Please, try again.'));
        }
        $this->set(compact('action'));
    }

    public function edit($id = null) {
        $action = $this->Actions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $action = $this->Actions->patchEntity($action, $this->request->getData());
            if ($this->Actions->save($action)) {
                $this->Flash->success(__('The action has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The action could not be saved. Please, try again.'));
        }
        $this->set(compact('action'));
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $action = $this->Actions->get($id);
        if ($this->Actions->delete($action)) {
            $this->Flash->success(__('The action has been deleted.'));
        } else {
            $this->Flash->error(__('The action could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    
    public function isAuthorized($user){
        if ($user['tipo'] == 1) { return true; }
    }
}
